package com.redes_csr30_utfpr;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public final class WebServer 
{
    private Integer port;
    private ServerSocket serverSocket;

    public Integer getPort() {
        return this.port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public ServerSocket getServer() {
        return this.serverSocket;
    }

    public void setServer(ServerSocket socket) {
        this.serverSocket = socket;
    }

    public WebServer(int port) {
        this.setPort(port);
    }

    public void start() {
        try {
            this.serverSocket = new ServerSocket(this.port);
        } catch (IOException e) {
            e.printStackTrace();
        }

        while(true) {
            try (Socket clientSocket = this.serverSocket.accept();) {
                HttpRequest request = new HttpRequest(clientSocket);
                Thread requestThread = new Thread(request);
                requestThread.start();
            }catch (Throwable t) {
                t.printStackTrace();
            }
        }
    }
    

}
